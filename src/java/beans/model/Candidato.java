package beans.model;

import javax.annotation.ManagedBean;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author tinix
 */
@ManagedBean
@Named(value = "candidato")
@RequestScoped
public class Candidato {

   
    public Candidato() {
    }
    
    private String nombre = "";

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    public Object getNombre(String nombre) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }  
    
    private String apellido = "Introduce Apellido";

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
        private String sueldoDeseado;

    public String getSueldoDeseado() {
        return sueldoDeseado;
    }

    public void setSueldoDeseado(String sueldoDeseado) {
        this.sueldoDeseado = sueldoDeseado;
    }


}